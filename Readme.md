# Device Backup Script

Simple script that takes input from a file containing a list of devices in
csv format, logs into the device using ssh, executes a specified command
(to generate text output which is captured) and saves to output to a text
file in a specified location.

**Current supported devices: Mikrotik, Ubiquiti Airmax/AirFiber radios,
Cambium ePMP radios, HP Switches**

Uses netmiko and paramiko packages, so can easily be extended.


## Installation instructions for:
### Linux:
    * Install git python3 pip virtualenv
    * Git clone
        cd /opt
        git clone https://Lupusmagist@bitbucket.org/Lupusmagist/device_backup.git
    * Virtualenv
        cd device_backup
        virtualenv -p python3 venv
    * Install Dependencies
        source venv/bin/activate
        pip install -r requirements.txt
    * Create a device_list.txt file based of the example and test while
        in the virtual environment.
        cp device_list.example device_list.txt
        // Edit file and add your device/s to test with as well as
            the path to store the backups to.
        chmod +x Backup.py              // Making file executable.
        ./Backup.py device_list.txt
    * Cron
        crontab -e
        // Run backup every Sunday
        * * * * 0 cd /opt/device_backup && /opt/device_backup/env/bin/python /opt/device_backup/Backup.py device_list.txt

### Windows
    * Install python3
    * Download repository zip file and extract to project folder
    * On command line run:
        cd /project folder
        pip install -r requirements.txt
    * Rename device_list.example to device_list.txt and edit to suit your devices
    * Add Backup.py script to Windows scheduler

##### Bugs:
When no input file is given, I need to throw a sensible error.
Check that comments and blank lines in middle of device_list will be processed correctly.
