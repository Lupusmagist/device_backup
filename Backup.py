#! /usr/bin/env python3
""" Module to read lines from a csv file to backup Mikrotik Routers,
    Ubiquiti Radios and HP Switches.

    Required input:
        Path to csv file with list of devices to backup
        e.g.:
        Windows: Backup.py c:\temp\device_list.txt
        Linux: Backup.py /tmp/device_list.txt

    Operation:
    The backup is made by login into the remote device using ssh and executing
    the command specified in the csv file. The output of this command is saved
    to a text file in the specified location.

    A log file, log.txt, is created in the folder that the script is run from.

    CSV File:
    One device per line in input file.
    No blank lines. (they will give a error)

    Name        - Name for the unit, a unique name, duplicates will be overwritten
    Type        - Type Codes:
                  1 = Mikrotik
                  2 = HP
                  3 = UBNT
                  4 = Cambium ePMP
    IP Address  - The IP of the device
    username    - Username to login to the device
    password    - password to login to the device
    command     - Command to execute on the device to generate a text output
                    HP: show running-config
                    Mikrotik: export compact
                    Ubiquiti: cat /tmp/system.cfg
                    Cambium: config show json
    location    - location to store the backup file.

    e.g.: device1,1,192.168.1.1,backupuser,backuppass,show running-config,c:\temp

    The location specified is OS specific. So Windows will follow c:\path and
    *nix will follow /path
"""
import csv
import sqlite3
import sys
import os
from datetime import datetime
from device_backup import Device_Backup

db = sqlite3.connect(':memory:')


def init_db(cur):
    cur.execute('''CREATE TABLE Device_List (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        Name TEXT,
        Type INTEGER,
        IP TEXT,
        User TEXT,
        Pass TEXT,
        Command TEXT,
        Location TEXT)''')


def populate_db(cur, csv_fp):
    rdr = csv.reader(filter(lambda row: row[0] != '#', csv_fp))
    cur.executemany(
        '''
        INSERT INTO Device_List (Name, Type, IP, User, Pass, Command, Location)
        VALUES (?,?,?,?,?,?,?)''', rdr)


def main(path):
    cur = db.cursor()
    init_db(cur)
    populate_db(cur, open(path))
    db.commit()
    cur.execute("SELECT * FROM Device_List")
    rows = cur.fetchall()

    # Once in the db, run the backup function for each line.
    try:
        filestring = os.path.dirname(
            os.path.realpath(__file__)) + '/' + "log" + '.txt'
        f = open(filestring, 'a')
    except IOError:
        print("Couldn't open % s", filestring)
    for row in rows:
        try:
            Device_Backup(row[1], row[2], row[3], row[4], row[5], row[6],
                          row[7])
            f.write('%-10s %-20s %-15s %-10s \n' % (
                datetime.now().strftime("%d%m%Y"), row[1], row[3], "Complete"))
            # print(row)
        except Exception:
            f.write(
                '%-10s %-20s %-15s %-10s \n' %
                (datetime.now().strftime("%d%m%Y"), row[1], row[3], "Error!!"))
            # print(row)
    f.close()


if __name__ == "__main__":
    main(sys.argv[1])
    # main('c:/temp/data.txt')
