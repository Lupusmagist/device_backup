from netmiko import ConnectHandler
import paramiko
from datetime import datetime
import os


def Device_Backup(Name, Type, IP, User, Pass, Command, Location):
    """Creates a backup based on the arguments provided.

    Arguments:
        Name - Device name
        ID   - Type of device. (1=Mikrotik, 2=HP, 3=Ubiquiti)
        IP   - IP address of device
        User - Username to log into device
        Pass - Password to log into device
        Command - Command to remotely execute on device
        Location - Location to save to output file.

    Returns:
        Saves a file to disk.

    Requires:
        Paramiko
        Netmiko
        Datetime
        OS

    Notes:
        Command to execute must have the device output text to capture.
            HP: show running-config
            Mikrotik: export compact
            Ubiquiti: cat /tmp/system.cfg
    """

    # We asume ssh is on port 22
    port = 22

    if int(Type) == 1 or int(Type) == 3 or int(Type) == 4:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(IP, port, User, Pass)

            stdin, stdout, stderr = ssh.exec_command("print")
            stdin, stdout, stderr = ssh.exec_command(Command)
            outlines = stdout.readlines()
            resp = ''.join(outlines)
            # print(resp)
            ssh.close()

        except paramiko.AuthenticationException:
            print("Authentication Failed - " + IP)
        except:
            print("Unknown error - " + IP)
            Name = "Error" + Name
    elif int(Type) == 2:
        net_connect = ConnectHandler(device_type='hp_procurve', ip=IP,
                                     username=User, password=Pass)
        resp = net_connect.send_command(Command)
        net_connect.disconnect()

    if int(Type) == 1 or int(Type) == 2:
        FileExt = '.txt'
    elif int(Type) == 3:
        FileExt = '.cfg'
    elif int(Type) == 4:
        FileExt = '.json'

    try:
        if not os.path.exists(Location):
            os.makedirs(Location)
        filestring = Location + '/' + \
            Name + '-' + IP + '-' + \
            datetime.now().strftime("%d%m%Y") + FileExt
        f = open(filestring, 'w')
        f.write(resp)
        f.close()
    except IOError:
        print("Couldn't open " + filestring)
